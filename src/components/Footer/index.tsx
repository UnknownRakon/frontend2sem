import { Paper, Typography } from '@mui/material'
import React from 'react'

const Footer = () => {
  return (
    <Paper
      sx={{
        borderRadius: 0,
        padding: 1,
      }}
      variant='outlined'
    >
      <Typography textAlign='center' variant='body1'>
        Курсовая работа студента группы 201-321 Воробьёва Владислава
      </Typography>
    </Paper>
  )
}
export default Footer
