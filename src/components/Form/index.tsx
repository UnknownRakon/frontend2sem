import {
  Autocomplete as MuiAutocomplete,
  Button,
  ButtonProps,
  Grid,
  TextField,
  TextFieldProps,
} from '@mui/material'
import type { DetailedHTMLProps, FC, FormHTMLAttributes } from 'react'
import { Controller } from 'react-hook-form'

type Option = { id: string; name: string }

export const Form: FC<DetailedHTMLProps<FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>> = ({
  children,
  ...props
}) => {
  return (
    <form {...props} noValidate>
      {children}
    </form>
  )
}

type Pattern = {
  value: RegExp
  message: string
}

type InputProps = {
  name: string
  control: any
  pattern?: Pattern
  validate?: any
} & TextFieldProps
export const Input: FC<InputProps> = ({ name, control, pattern, validate, ...props }) => {
  return (
    <Controller
      name={name}
      control={control}
      // eslint-disable-next-line react/prop-types
      rules={{ required: props.required, pattern: pattern, validate: validate }}
      render={({ field }: { field: any }) => <TextField {...field} {...props} />}
    />
  )
}

export type ComboboxProps = {
  name: string
  control: any
  label: string
  placeholder: string
  required?: boolean
  error?: boolean
  options: (Option | undefined | null)[] | undefined | null
  ButtonProps?: ButtonProps
  disabled?: boolean
}

export const Combobox: FC<ComboboxProps> = ({
  name,
  control,
  label,
  required,
  error,
  options,
  placeholder,
  ButtonProps,
  disabled,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{ required }}
      render={({ field }: { field: any }) => (
        <MuiAutocomplete
          {...field}
          disabled={disabled}
          options={options}
          onChange={(_, data) => field.onChange(data)}
          value={field.value}
          renderInput={(params) => (
            <Grid container columnSpacing={1} wrap='nowrap'>
              <Grid item width='100%'>
                <TextField
                  {...params}
                  label={label}
                  placeholder={placeholder}
                  required={required}
                  error={error}
                />
              </Grid>
              {ButtonProps && (
                <Grid item>
                  <Button disableElevation sx={{ height: 40, mt: '23px' }} {...ButtonProps} />
                </Grid>
              )}
            </Grid>
          )}
        />
      )}
    />
  )
}

type TextareaProps = { name: string; control: any } & TextFieldProps
export const Textarea: FC<TextareaProps> = ({ name, control, defaultValue, ...props }) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{ required: props.required }}
      defaultValue={defaultValue}
      render={({ field }) => <TextField multiline={true} minRows={3} {...field} {...props} />}
    />
  )
}
