import { Button, Grid, Typography } from '@mui/material'
import { useStore } from 'effector-react'
import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

import { BounceArrow } from '../../components'
import Paths from '../../routes'
import { $hasNextVacancies, $vacancies, getVacanciesFx, resetVacancies } from '../../store'
import { VacancyCard } from './components'

const VacancyPage = () => {
  const vacancies = useStore($vacancies)
  const hasNext = useStore<boolean>($hasNextVacancies)
  const navigate = useNavigate()

  let offset = 0

  useEffect(() => {
    if (hasNext) {
      const win: Window = window
      win.addEventListener('scroll', handleScroll)
    }

    return () => {
      unsubscribe()
    }
  }, [hasNext])

  useEffect(() => {
    getVacanciesFx(offset.toString())
    return () => resetVacancies()
  }, [])

  const unsubscribe = () => window.removeEventListener('scroll', handleScroll)

  const handleScroll = (e: Event) => {
    const targetEl: Document = e?.target as Document

    const scrollHeight = targetEl.documentElement.scrollHeight
    const currentHeight = Math.ceil(targetEl.documentElement.scrollTop + window.innerHeight)

    if (currentHeight + 1 >= scrollHeight && hasNext) {
      offset += 5
      getVacanciesFx(offset.toString())
    }
  }
  if (!vacancies.length)
    return (
      <Typography variant='h3' textAlign='center'>
        Загрузка...
      </Typography>
    )
  return (
    <Grid container direction='column' rowGap={2} sx={{ padding: 2 }}>
      <Grid item alignSelf='center'>
        <Button onClick={() => navigate(Paths.FORM)}>Добавить вакансию</Button>
      </Grid>
      {vacancies.map((v) => (
        <VacancyCard key={v.id} title={v.title} description={v.description} />
      ))}
      {hasNext && (
        <Grid
          item
          container
          alignItems='center'
          direction='column'
          sx={{ margin: '2em 0 8em' }}
          rowGap={1}
        >
          <Grid item>
            <Typography variant='h2'>Далее</Typography>
          </Grid>
          <Grid item>
            <BounceArrow />
          </Grid>
        </Grid>
      )}
    </Grid>
  )
}
export default VacancyPage
