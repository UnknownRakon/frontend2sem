import WorkspacesIcon from '@mui/icons-material/Workspaces'
import { Card, Grid, Typography } from '@mui/material'
import { FC } from 'react'

type CardProp = {
  title: string
  description: string
}

const VacancyCard: FC<CardProp> = ({ title, description }) => {
  return (
    <Grid item>
      <Card variant='outlined'>
        <Grid container columnGap={2}>
          <Grid item>
            <WorkspacesIcon sx={{ fontSize: 162 }} />
          </Grid>
          <Grid item sx={{ paddingTop: 4 }}>
            <Grid container direction='column' rowGap={3}>
              <Grid item>
                <Typography variant='h2'>Название: {title}</Typography>
              </Grid>
              {!!description.length && (
                <Grid item>
                  <Typography variant='h3'>Описание: {description}</Typography>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Card>
    </Grid>
  )
}
export default VacancyCard
