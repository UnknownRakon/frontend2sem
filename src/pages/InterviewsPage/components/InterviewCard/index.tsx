import MicIcon from '@mui/icons-material/Mic'
import { Card, Grid, Typography } from '@mui/material'
import { FC } from 'react'

type CardProp = {
  status: string
  date: string
}

enum Status {
  PLANNED = 'PLANNED',
  PROCESSING = 'PROCESSING',
}

const InterviewCard: FC<CardProp> = ({ status, date }) => {
  return (
    <Grid item>
      <Card variant='outlined'>
        <Grid container columnGap={2}>
          <Grid item>
            <MicIcon sx={{ fontSize: 162 }} />
          </Grid>
          <Grid item sx={{ paddingTop: 4 }}>
            <Grid container direction='column' rowGap={3}>
              <Grid item>
                <Typography variant='h2'>
                  Статус: {status === Status.PLANNED ? 'Планируемое' : 'В процессе'}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant='h3'>Дата: {new Date(date).toDateString()}</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Card>
    </Grid>
  )
}
export default InterviewCard
