import { Button, ButtonGroup, Grid, Typography } from '@mui/material'
import { useStore } from 'effector-react'
import React, { useEffect } from 'react'

import { BounceArrow } from '../../components'
import {
  $employers,
  $hasNextInterviews,
  $interviews,
  getAllEmployersFx,
  getFilteredInterviewsFx,
  getInterviewsFx,
  resetEmployers,
  resetInterviews,
} from '../../store'
import { InterviewCard } from './components'

const InterviewPage = () => {
  const interviews = useStore($interviews)
  const companies = useStore($employers)
  const hasNext = useStore<boolean>($hasNextInterviews)
  let offset = 0

  useEffect(() => {
    if (hasNext) {
      const win: Window = window
      win.addEventListener('scroll', handleScroll)
    }

    return () => {
      unsubscribe()
    }
  }, [hasNext])

  useEffect(() => {
    getInterviewsFx(offset.toString())
    getAllEmployersFx()
    return () => {
      resetEmployers(), resetInterviews()
    }
  }, [])

  console.log(companies)

  const unsubscribe = () => window.removeEventListener('scroll', handleScroll)

  const handleScroll = (e: Event) => {
    const targetEl: Document = e?.target as Document

    const scrollHeight = targetEl.documentElement.scrollHeight
    const currentHeight = Math.ceil(targetEl.documentElement.scrollTop + window.innerHeight)

    if (currentHeight + 1 >= scrollHeight && hasNext) {
      offset += 5
      getInterviewsFx(offset.toString())
    }
  }

  const handleFilter = (id: string) => {
    unsubscribe()
    resetInterviews()
    getFilteredInterviewsFx(id)
  }

  const handleAll = () => {
    resetInterviews()
    offset = 0
    getInterviewsFx(offset.toString())
  }

  return (
    <Grid container direction='column' rowGap={2} sx={{ padding: 2 }}>
      <Grid item container justifyContent='center'>
        <ButtonGroup variant='contained' aria-label='outlined primary button group'>
          <Button onClick={() => handleAll()}>ВСЕ</Button>
          {companies.map((c) => (
            <Button onClick={() => handleFilter(c.id.toString())} key={c.id}>
              {c.name}
            </Button>
          ))}
        </ButtonGroup>
      </Grid>
      {!!interviews.length &&
        interviews.map((i) => <InterviewCard key={i.id} status={i.status} date={i.date} />)}
      {!interviews.length && (
        <Typography variant='h2' textAlign='center'>
          Интервью отсутствуют
        </Typography>
      )}
      {hasNext && (
        <Grid
          item
          container
          alignItems='center'
          direction='column'
          sx={{ margin: '2em 0 8em' }}
          rowGap={1}
        >
          <Grid item>
            <Typography variant='h2'>Далее</Typography>
          </Grid>
          <Grid item>
            <BounceArrow />
          </Grid>
        </Grid>
      )}
    </Grid>
  )
}
export default InterviewPage
