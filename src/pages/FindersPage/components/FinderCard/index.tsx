import PersonIcon from '@mui/icons-material/Person'
import { Card, Grid, Typography } from '@mui/material'
import { FC } from 'react'

type CardProp = {
  name: string
  lastname: string
}

const FinderCard: FC<CardProp> = ({ name, lastname }) => {
  return (
    <Grid item>
      <Card variant='outlined'>
        <Grid container columnGap={2}>
          <Grid item>
            <PersonIcon sx={{ fontSize: 162 }} />
          </Grid>
          <Grid item sx={{ paddingTop: 4 }}>
            <Grid container direction='column' rowGap={3}>
              <Grid item>
                <Typography variant='h2'>Имя: {name}</Typography>
              </Grid>
              <Grid item>
                <Typography variant='h3'>Фамилия: {lastname}</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Card>
    </Grid>
  )
}
export default FinderCard
