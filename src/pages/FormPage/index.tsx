import { Button, Grid, Typography } from '@mui/material'
import { useStore } from 'effector-react'
import { useEffect } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'

import { Combobox, Form, Input, Textarea } from '../../components'
import Paths from '../../routes'
import { $employers, getAllEmployersFx, postVacancyFx, resetEmployers } from '../../store'

type FormData = {
  title: string
  description: string
  company: {
    id: string
    name: string
  } | null
}

const FormPage = () => {
  const companies = useStore($employers)

  const navigate = useNavigate()

  useEffect(() => {
    getAllEmployersFx()
    return () => {
      resetEmployers()
    }
  }, [])

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<FormData>({
    mode: 'onChange',
    defaultValues: {
      title: '',
      description: '',
      company: null,
    },
  })

  const submitHandler: SubmitHandler<FormData> = async (data) => {
    try {
      const { title, description, company } = data

      await postVacancyFx({ title, description, company: company?.id }).then(() =>
        navigate(Paths.VACANCIES),
      )
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Grid container direction='column' rowGap={2} alignItems='center'>
      <Grid item sx={{ width: '30%', paddingTop: 2 }}>
        <Form onSubmit={handleSubmit(submitHandler)}>
          <Grid container direction='column' rowSpacing={1}>
            <Grid item>
              <Typography variant='h2' textAlign='center'>
                Создание вакансии
              </Typography>
            </Grid>
            <Grid item>
              <Input
                name='title'
                control={control}
                label='Название'
                placeholder='Введите название вакансии'
                required={true}
                error={!!errors.title}
              />
            </Grid>
            <Grid item>
              <Textarea
                name='description'
                control={control}
                label='Описание'
                placeholder=''
                error={!!errors.description}
              />
            </Grid>
            <Grid item>
              <Combobox
                name='company'
                control={control}
                options={companies.map((c) => ({ id: c.id.toString(), name: c.name }))}
                label='Форма взаимодействия'
                placeholder='Выберите форму взаимодействия'
                required={true}
                error={!!errors.company}
              />
            </Grid>
            <Grid item alignSelf='center'>
              <Button type='submit' disabled={!isValid}>
                Создать
              </Button>
            </Grid>
          </Grid>
        </Form>
      </Grid>
    </Grid>
  )
}
export default FormPage
