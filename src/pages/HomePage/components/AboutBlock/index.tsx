import { Grid, Typography } from '@mui/material'
import { Container } from '@mui/system'
import React from 'react'

const AboutBlock = () => {
  return (
    <>
      <Grid item>
        <img
          style={{
            maxWidth: '100%',
            height: 'auto',
            padding: 0,
            margin: 0,
          }}
          src='https://techrocks.ru/wp-content/uploads/2018/05/ultraInterview.jpg'
        />
      </Grid>
      <Grid item>
        <Typography variant='h2' textAlign='center'>
          О сервисе
        </Typography>
      </Grid>
      <Grid item>
        <Container maxWidth='md'>
          <Typography>
            СОБЕСедник — один из самых крупных сайтов по поиску работы и сотрудников в мире (по
            данным рейтинга Similarweb). Мы создаем передовые технологии на всех доступных
            платформах для того, чтобы работодатели могли быстро найти подходящего сотрудника, а
            соискатели — хорошую работу. Наши мобильные приложения стабильно занимают первые места в
            категории «Бизнес» на всех платформах. Наш поиск использует искусственный интеллект, а
            сайт обрабатывает до 3000 запросов в секунду. Каждый месяц на сайте появляется более
            полумиллиона вакансий, а технологии для работы с персоналом насчитывают более 30
            позиций. Ежедневно мы помогаем сотням тысяч человек изменить свою жизнь к лучшему.
          </Typography>
        </Container>
      </Grid>
    </>
  )
}
export default AboutBlock
