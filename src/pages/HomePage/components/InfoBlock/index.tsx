import { Grid, Typography } from '@mui/material'
import React from 'react'

const InfoBlock = () => {
  return (
    <Grid item container justifyContent='space-around' sx={{ padding: 5 }}>
      <Grid
        item
        container
        direction='column'
        justifyContent='space-evenly'
        sx={{ p: 2, width: 500 }}
      >
        <Typography variant='h2' textAlign='center' sx={{ m: 1 }}>
          Работа
        </Typography>
        <Typography variant='h5' textAlign='center'>
          СОБЕСедник - это лучшие предложения высокооплачиваемой работы от российских и зарубежных
          компаний.
        </Typography>
      </Grid>
      <Grid
        item
        container
        direction='column'
        justifyContent='space-evenly'
        sx={{ p: 2, width: 500 }}
      >
        <Typography variant='h2' textAlign='center' sx={{ m: 1 }}>
          Сотрудники
        </Typography>
        <Typography variant='h5' textAlign='center'>
          СОБЕСедник - самая большая и качественная база резюме лучших специалистов в России.
        </Typography>
      </Grid>
    </Grid>
  )
}
export default InfoBlock
