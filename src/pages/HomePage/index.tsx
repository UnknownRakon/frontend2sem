import { Grid } from '@mui/material'

import { AboutBlock, InfoBlock } from './components'

const HomePage = () => (
  <Grid container direction='column' rowGap={2}>
    <InfoBlock />
    <AboutBlock />
  </Grid>
)
export default HomePage
