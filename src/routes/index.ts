enum Paths {
  EMPLOYERS = '/employers',
  FINDERS = '/finders',
  VACANCIES = '/vacancies',
  INTERVIEWS = '/interviews',
  MAIN = '/',
  FORM = '/form',
}
export default Paths
