import { ThemeProvider } from '@mui/material'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

import App from './App'
import { EmployersPage, FindersPage, FormPage, InterviewsPage, VacancyPage } from './pages'
import HomePage from './pages/HomePage'
import Paths from './routes'
import theme from './theme'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}>
          <Route index element={<HomePage />} />
          <Route path={Paths.EMPLOYERS} element={<EmployersPage />} />
          <Route path={Paths.FINDERS} element={<FindersPage />} />
          <Route path={Paths.VACANCIES} element={<VacancyPage />} />
          <Route path={Paths.INTERVIEWS} element={<InterviewsPage />} />
          <Route path={Paths.FORM} element={<FormPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </ThemeProvider>,
)
